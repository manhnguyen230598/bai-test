
import { useState, useEffect } from 'react';
import { DatePicker } from 'antd'
import 'antd/dist/antd.css'
import moment from 'moment'


function NewTask(props) {

  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

  const [titleTask, setTitleTask] = useState("")
  const [piority, setPiority] = useState("Low")
  const [note, setNote] = useState("")
  const [dueDate, setDueDate] = useState(date)
  const [listArrayTask, setListArrayTask] = useState([])
  const [id, setId] = useState(0)

  

  function disabledDate(current) {
    // Can not select days before today and today
    return current < moment().add(-1,'days')
  }
  
  const AddTask = () => {
    if(titleTask === ""){
      alert("Vui lòng điền tên công việc")
    } else {
      const arrTask = {
        titleTask : titleTask,
        note : note,
        piority : piority,
        dueDate : dueDate,
        id : id
      }
      const array = [...listArrayTask]
      array.push(arrTask)
      setListArrayTask(array)
      setId(id + 1)
      alert("Thêm task thành công, vui lòng load lại trang")
    }
  }

  function handleDated(date, dateString) {
    setDueDate(dateString)
  }
  useEffect(() => {
    if(listArrayTask.length != 0 ){
      localStorage.setItem("listTask", JSON.stringify(listArrayTask))
      setId(listArrayTask[listArrayTask.length -1].id +1)
    } else {
      if(localStorage.getItem("listTask")){
        setListArrayTask(JSON.parse(localStorage.getItem("listTask")))
      }
    }
  }, [listArrayTask])

  
  return (
   <>
     <div className='c-new-task'>
       <div className='c-new-task__title'>
        <b>New Task</b>
       </div>
       <div className='c-new-task__task'>
        <input type='text' placeholder='Add new task ...' onChange={(e) => setTitleTask(e.target.value)}/>
       </div>
       <div className='c-new-task__note'>
        <b>Description</b>
        <textarea rows="5" onChange={(e) => setNote(e.target.value)}/>
       </div>
       <div className='c-new-task__calander'>
         <div className='c-new-task__calander__content'>
           <b>Due Date</b>
           <DatePicker className='c-new-task__calander__content__date'
            disabledDate={disabledDate}
            onChange={handleDated}
            format="YYYY-MM-DD"
            defaultValue={moment(date , 'YYYY-MM-DD')}
           />
         </div>
         <div className='c-new-task__calander__content'>
           <b>Piority</b>
           <select onChange={(e) => setPiority(e.target.value)}>
             <option>Low</option>
             <option>Normal</option>
             <option>High</option>
           </select>
         </div>
       </div>
       <div className='c-new-task__btn' onClick={() => AddTask()}>
         Add
       </div>
     </div>
   </>
  )
}


export default NewTask;