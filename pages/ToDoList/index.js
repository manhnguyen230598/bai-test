
import { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { DatePicker } from 'antd'
import 'antd/dist/antd.css'
import moment from 'moment'


function DoList(props) {

  const [checkId, setCheckId] = useState('')
  const [listTask, setListTask] = useState([])
  const [taskUpdate, setTaskUpdate] = useState([])
  const [showBulk, setShowBulk] = useState(true)
  const [listRemove, setListRemove] = useState([])
  const [mapList, setMapList] = useState([])
  const [searchTask, setSearchTask] = useState(mapList)
  const [search, setSearch] = useState('')


  useEffect(() => {
    if (localStorage.getItem("listTask")) {
      const array = JSON.parse(localStorage.getItem("listTask"))
      setMapList(array.sort((a, b) => new Date(a.dueDate) - new Date(b.dueDate)))
      setListTask(array)
    }
  }, [])

  const CheckShowDetai = (id) => {
    if (checkId === id) {
      setCheckId('')
    }
  }

  const RemoveTask = (id) => {
    const new_arr = listTask.filter(item => item.id != id)
    if(new_arr.length === 0){
      localStorage.removeItem("listTask")
    } else {
      for (let i = 0; i < new_arr.length; i++) {
        if (new_arr[i].status) {
          new_arr[i].status = null
        }
      }
      localStorage.setItem("listTask", JSON.stringify(new_arr))
    }
    window.location.reload();
  }

  console.log(listTask , 222222)


  const handleInputChange = (index, event) => {
    const values = [...listTask]
    if (event.target.name === 'titleTask') {
      values[index].titleTask = event.target.value
    }
    if (event.target.name === 'note') {
      values[index].note = event.target.value
    }
    if (event.target.name === 'piority') {
      values[index].piority = event.target.value
    }
    if (event.target.name === 'dueDate') {
      values[index].dueDate = event.target.value
    }
    setTaskUpdate(values)

  }

  const handleDateChange = (index, event) => {
    const values = [...listTask]
    values[index].dueDate = event
    setTaskUpdate(values)
  }

  const UpdateTask = () => {
    const updateTask = [...taskUpdate]
    if (updateTask.some((e) => e.titleTask === '')) {
      alert('Tên task không được để trống')
    } else {
      localStorage.setItem("listTask", JSON.stringify(taskUpdate))
      window.location.reload();
    }

  }

  function disabledDate(current) {
    // Can not select days before today and today
    return current < moment().add(-1, 'days')
  }

  const CheckActive = (id) => {
    var values = [...listTask];
    let index = values.findIndex(i => i.id == id);

    if (index >= 0) {
      if (!values[index].status) {
        values[index].status = 'active';
      } else {
        values[index].status = null;
      }
      setListTask(values);
    }
  }

  useEffect(() => {
    const arrayRemove = []
    if (listTask) {
      for (let i = 0; i < listTask.length; i++) {
        if (listTask[i].status === "active") {
          setShowBulk(false)
          break;
        } else {
          setShowBulk(true)
        }
      }
      for (let i = 0; i < listTask.length; i++) {
        if (listTask[i].status != "active") {
          arrayRemove.push(listTask[i])
        }
      }
      
    }
    setListRemove(arrayRemove)
  }, [listTask])

  const RemoveTaskBulk = () => {
    if(listRemove.length === 0){
      localStorage.removeItem("listTask")
    } else {
      localStorage.setItem("listTask", JSON.stringify(listRemove))
    }
    window.location.reload();
  }


  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    setSearch(value)
    result = mapList.filter((data) => {
      return data.titleTask.search(value) != -1;
    });
    setSearchTask(result);
  }

  return (
    <>
      <div className='c-new-task is-todo-list'>
        <div className='c-new-task__title'>
          <b>To Do List</b>
        </div>
        <div className='c-new-task__task'>
          <input type='text' placeholder='Search ...' onChange={(event) => handleSearch(event)}/>
        </div>
        {search === '' ? 
        <>{mapList ? mapList.map((value, idx) => {
          return (
            <div key={idx}>
              <div className='c-new-task__istask' >
                <div className='c-new-task__istask__top'>
                  <input type="checkbox" onClick={(e) => CheckActive(value.id)} />
                  <span>
                    {value.titleTask}
                  </span>
                  <div className='c-new-task__istask__top__btn'>
                    <button onClick={() => { setCheckId(value.id), CheckShowDetai(value.id) }}>Detail</button>
                    <button onClick={() => RemoveTask(value.id)}>Remove</button>
                  </div>
                </div>
                <div className='c-new-task__istask__bot' style={{ display: `${value.id === checkId ? "" : "none"}` }}>
                  <div className='c-new-task__task'>
                    <input type='text' defaultValue={value.titleTask}
                      name='titleTask'
                      onChange={(event) =>
                        handleInputChange(idx, event)}
                    />
                  </div>
                  <div className='c-new-task__note'>
                    <b>Description</b>
                    <textarea rows="5" defaultValue={value.note}
                      name='note'
                      onChange={(event) =>
                        handleInputChange(idx, event)} />
                  </div>
                  <div className='c-new-task__calander'>
                    <div className='c-new-task__calander__content'>
                      <b>Due Date</b>
                      <DatePicker className='c-new-task__calander__content__date'
                        disabledDate={disabledDate}
                        defaultValue={value.dueDate ? moment(value.dueDate , 'YYYY-MM-DD') : ''}
                        name='dueDate'
                        onChange={(e) =>
                          handleDateChange(
                            idx,
                            moment(e).format('YYYY-MM-DD')
                          )
                        }
                      />
                    </div>
                    <div className='c-new-task__calander__content'>
                      <b>Piority</b>
                      <select defaultValue={value.piority}
                        name='piority'
                        onChange={(event) =>
                          handleInputChange(idx, event)}>
                        <option>Low</option>
                        <option>Normal</option>
                        <option>High</option>
                      </select>
                    </div>
                    <div className='c-new-task__btn'
                      onClick={() => UpdateTask()}
                    >
                      Update
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        }) : null}</> :
        <>{searchTask.length != 0 ? searchTask.map((value, idx) => {
          return (
            <div key={idx}>
              <div className='c-new-task__istask' >
                <div className='c-new-task__istask__top'>
                  <input type="checkbox" onClick={(e) => CheckActive(value.id)} />
                  <span>
                    {value.titleTask}
                  </span>
                  <div className='c-new-task__istask__top__btn'>
                    <button onClick={() => { setCheckId(value.id), CheckShowDetai(value.id) }}>Detail</button>
                    <button onClick={() => RemoveTask(value.id)}>Remove</button>
                  </div>
                </div>
                <div className='c-new-task__istask__bot' style={{ display: `${value.id === checkId ? "" : "none"}` }}>
                  <div className='c-new-task__task'>
                    <input type='text' defaultValue={value.titleTask}
                      name='titleTask'
                      onChange={(event) =>
                        handleInputChange(idx, event)}
                    />
                  </div>
                  <div className='c-new-task__note'>
                    <b>Description</b>
                    <textarea rows="5" defaultValue={value.note}
                      name='note'
                      onChange={(event) =>
                        handleInputChange(idx, event)} />
                  </div>
                  <div className='c-new-task__calander'>
                    <div className='c-new-task__calander__content'>
                      <b>Due Date</b>
                      <DatePicker className='c-new-task__calander__content__date'
                        disabledDate={disabledDate}
                        defaultValue={value.dueDate ? moment(value.dueDate , 'YYYY-MM-DD') : ''}
                        name='dueDate'
                        onChange={(e) =>
                          handleDateChange(
                            idx,
                            moment(e).format('YYYY-MM-DD')
                          )
                        }
                      />
                    </div>
                    <div className='c-new-task__calander__content'>
                      <b>Piority</b>
                      <select defaultValue={value.piority}
                        name='piority'
                        onChange={(event) =>
                          handleInputChange(idx, event)}>
                        <option>Low</option>
                        <option>Normal</option>
                        <option>High</option>
                      </select>
                    </div>
                    <div className='c-new-task__btn'
                      onClick={() => UpdateTask()}
                    >
                      Update
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        }) : "Không tìm thấy task bạn tìm kiếm"}</>
        }
        <div className='c-new-task__bot' style={{ display: `${showBulk ? "none" : ""}` }}>
          <div className='c-new-task__bot__left'>
            Bulk Action:
          </div>
          <div className='c-new-task__bot__right'>
            <button>Done</button>
            <button onClick={() => RemoveTaskBulk()}>Remove</button>
          </div>
        </div>
      </div>
    </>
  )
}


export default DoList;