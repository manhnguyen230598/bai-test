import "bootstrap/dist/css/bootstrap.min.css";
import "animate.css";
import "font-awesome/css/font-awesome.min.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import App from 'next/app';
import { Provider } from 'react-redux';
// import '../styles/globals.css'
import '../styles/static/css/style.css'
import React from 'react';
// import withRedux from "next-redux-wrapper";
import store from '../redux/store';

const MyApp = ({ Component, pageProps }) => {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
};

/* MyApp.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);
  return { ...appProps };
}; */

//makeStore function that returns a new store for every request
// const makeStore = () => store;

//withRedux wrapper that passes the store to the App Component
export default MyApp;
