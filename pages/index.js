
import { useState, useEffect } from 'react';
import { connect } from "react-redux";
import NewTask from './NewTask';
import ToDoList from './ToDoList'

function Home(props) {
  return (
   <>
   <div className='c-home'>
     <div className='c-home__left'>
     <NewTask/>
     </div>
    <div className='c-home__right'>
    <ToDoList/>
    </div>
    
    </div>
   </>
  )
}

export default Home;