import React from 'react';
import Link from 'next/link'
import { connect } from "react-redux";
import Fade from 'react-reveal/Fade';

function Footer(props) {
    const { language } = props
    return (
        <>
            <Fade big delay={500}>
                <div className="l-footer">
                    <div className="container">
                        <div className="c-footer">
                            <div className="c-footer__top">
                                <div className="row">
                                    <div className="col-md-9">
                                        <i className="icon-multiply"></i><span>{language === "VN" ? "Let us do anything you need for a brand." : "Let us do anything you need for a brand."}</span>
                                    </div>
                                    <div className="col-md-3">
                                        <Link href={'/contact-us'}>{language === "VN" ? "Liên hệ với chúng tôi" : "Contact us"}</Link>
                                    </div>
                                </div>
                            </div>
                            <div className="c-footer__bottom row">
                                {language === "VN" ?
                                    <div className="c-footer__bottom__top col-md-5">
                                        <p>Làm việc tại Hà Nội</p>
                                        <p>4B, ngõ Trần Xuân Soạn, </p>
                                        <p>phố Trần Xuân Soạn</p>
                                        <p>Hai Bà Trưng.</p>
                                        <br />
                                        <br />
                                        <p>0242 321 0066 / 0242 248 822</p>
                                        {/* <p>do.creative@agency</p> */}
                                    </div>
                                    :
                                    <div className="c-footer__bottom__top col-md-5">
                                        <p>Made in Hanoi</p>
                                        <p>4B Tran Xuan Soan Alley,</p>
                                        <p>Hai Ba Trung.</p>
                                        <br />
                                        <br />
                                        <p>0242 321 0066 / 0242 248 822</p>
                                        {/* <p>do.creative@agency</p> */}
                                    </div>
                                }
                                <div className="c-footer__bottom__bottom col-md-12">
                                    <ul className="row">
                                        <li className="col-12 col-md-3 col-lg-4">
                                            <a href="/" target="_blank">agency.docreative@gmail.com</a>
                                        </li>
                                        <li className="col-6 col-md-2 ">
                                            <a href="https://www.facebook.com/docreative.vn" target="_blank">Facebook</a>
                                        </li>
                                        <li className="col-6 col-md-2">
                                            <a href="https://www.instagram.com/do.creative.agency/" target="_blank">Instagram</a>
                                        </li>
                                        <li className="col-6 col-md-2">
                                            <a href="https://www.behance.net/docreative1" target="_blank">Behance</a>
                                        </li>
                                        <li className="col-12 col-md-3 col-lg-2">
                                            Copyright 2020
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fade>
        </>
    );
}

const mapStateToProps = state => {
    return {
        language: state.changeLanguage
    };
};
export default connect(mapStateToProps, null)(Footer);