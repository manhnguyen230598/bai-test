import React, { useState } from 'react';
import Link from 'next/link'
import $ from "jquery";
import { connect } from "react-redux";
import { actLanguage } from "../../redux/actions"

function Header(props) {
    const { language, onLanguage } = props
    const [menuOpen, setMenuOpen] = useState(false)
    const openMenu = () => {
        setMenuOpen(!menuOpen);
        var body = $('body');
        var menu = $('.c-menu');
        var page = $('.l-nav');
        var ovelay = $('.c-app-ovelay');
        if (page.hasClass('has-page-open')) {
            page.removeClass('has-page-open');
            ovelay.removeClass('has-ovelay-show');
            menu.removeClass('has-menu-open');
            body.removeClass('has-body-open');
        } else {
            page.addClass('has-page-open');
            ovelay.addClass('has-ovelay-show');
            menu.addClass('has-menu-open');
            body.addClass('has-body-open');
        }
    }
    const closeMenu = () => {
        setMenuOpen(!menuOpen);
        var body = $('body');
        var menu = $('.c-menu');
        var page = $('.l-nav');
        var ovelay = $('.c-app-ovelay');
        page.removeClass("has-page-open");
        ovelay.removeClass("has-ovelay-show");
        menu.removeClass("has-menu-open");
        body.removeClass("has-body-open");
    }
    const setLanguageData = (data) => {
        onLanguage(data);
        // localStorage.setItem("LANGUAGE", JSON.stringify(data));
    }
    return (
        <>
            <div className="l-nav">
                <div className="container clearfix">
                    <div className="c-logo">
                        <Link href={'/'}>
                            <a>
                                <img src={"/images/logo.png"} alt="logo" />
                                <img src={"/images/logo_animation.png"} alt="logo" />
                            </a>
                        </Link>
                    </div>
                    <div className="c-right-menu">
                        <button className={`c-menu-expand js-menu-expand ${menuOpen ? " is-active" : ""}`} type="button" onClick={() => openMenu()}>
                            <span></span>
                        </button>
                        <div className="c-right-menu__pc">
                            <ul className="clearfix">
                                <li>
                                    <Link href={`/all-post/`}>
                                        {language === "VN" ? "Dự án" : "Works"}
                                    </Link>
                                </li>
                                <li>
                                    <Link href={"/contact-us"}>
                                        {language === "VN" ? "Liên hệ" : "Contact"}
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="c-menu">
                <span onClick={() => closeMenu()} className="js-app-close"></span>
                <div className="c-menu__box">
                    <div className="c-menu__box__language">
                        <ul className="clearfix">
                            <li onClick={() => {
                                setLanguageData("EN")
                                closeMenu()
                            }}>{language === "VN" ? "Tiếng Anh" : "English"}</li>
                            <li onClick={() => {
                                setLanguageData("VN")
                                closeMenu()
                            }}>{language === "VN" ? "Tiếng Việt" : "Vietnamse"}</li>
                        </ul>
                    </div>
                    <div className="c-menu__box__menu">
                        <ul className="clearfix">
                            <li onClick={() => closeMenu()}>
                                <Link href={'/'}>{language === "VN" ? "Trang chủ" : "Home"}</Link>
                            </li>
                            <li onClick={() => closeMenu()}>
                                <Link href={'/about-us'}>{language === "VN" ? "Về chúng tôi" : "About Us"}</Link>
                            </li>
                            <li onClick={() => closeMenu()}>
                                <Link href={'/all-post'}>{language === "VN" ? "Dự án" : "Works"}</Link>
                            </li>
                            <li onClick={() => closeMenu()}>
                                <Link href={'/contact-us'}>{language === "VN" ? "Liên hệ" : "Contact"}</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="c-menu__box__social">
                        <ul className="clearfix">
                            <li>
                                <a href="https://www.facebook.com/docreative.vn" target="_blank">Fb</a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/do.creative.agency/" target="_blank">Ins</a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/docreative1" target="_blank">Be</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="c-app-ovelay js-app-ovelay" onClick={() => closeMenu()}></div>
        </>
    );

}

const mapDispatchToProps = (dispatch) => {
    return {
        onLanguage: (data) => {
            dispatch(actLanguage(data))
        },
    }
}

const mapStateToProps = state => {
    return {
        language: state.changeLanguage
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Header);

