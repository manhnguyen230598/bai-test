import React from 'react';
import Header from './Header';
import Footer from './Footer';
import { connect } from "react-redux";
import Head from 'next/head'

function Layout({
    children,
    title = 'DoCreative',
    image = 'http://docreative.vn/images/banner-trang-chu.jpg'
}) {
    // const { setloading } = props
    return (
        <>
            <Head>
                <title>{title}</title>
                <link rel="icon" href="/favicon.ico" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta property="og:title" content={title} />
                <meta property="og:description" content={title} />
                <meta property="og:image" content={image} />
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
                <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
                <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                <script src="https://unpkg.com/scrollreveal"></script>
            </Head>
            <Header />
            <div id="c-scroll-smooth-id" className="c-scroll-smooth-id">
                <div className="l-nav-height"></div>
                {children}
                <Footer />
            </div>
            {/* {setloading ? <div className="c-build-loading"><div className="lds-ring"><div></div><div></div><div></div><div></div></div></div>
                : null} */}
        </>
    );

}

const mapStateToProps = state => {
    return {
        setloading: state.setLoading
    };
};
export default connect(mapStateToProps, null)(Layout);