import * as Types from "../constants/ActionTypes";

export const actLanguage = (data) => {
  return {
    type: Types.CHANGE_LANGUAGE,
    data: data
  }
}

export const actLoading = (data) => {
  return {
    type: Types.SET_LOADING,
    data: data
  }
}