import * as types from "../constants/ActionTypes";
import { useEffect } from "react";

var data = { language: "VN" };
var initailState = data ? data : {};

const changeLanguage = (state = initailState, action) => {
  // var { accessToken, userInfo } = action;
  var newState = { ...state };
  switch (action.type) {
    case types.CHANGE_LANGUAGE:
      newState = action.data;
      // localStorage.setItem("LANGUAGE", JSON.stringify(newState));
      return newState;
    default:
      return state;
  }
};

export default changeLanguage;
